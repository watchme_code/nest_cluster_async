# next-cluster

`next-cluster` represents a cutting-edge, distributed microservices template, born from the comprehensive understanding and principles acquired in the [Asynchronous Architecture Course](https://tough-dev.school/architecture). 

## Inspiration

The creation of `next-cluster` was motivated by the desire to apply and share the knowledge gained from the Asynchronous Architecture course. It showcases the application of asynchronous communication patterns, microservices architecture, and containerization, offering a practical example of the course's teachings.

## Key Features

- **Monorepository with Shared Library:** Integrates a monolithic codebase approach with the flexibility of microservices, promoting code reuse and easier maintenance.
- **Custom `package.json` and `Dockerfile` for Each Microservice:** Enables precise dependency management and container configurations, optimizing the development lifecycle.
- **Automated Docker Builds and Deployments:** Implements a tailor-made CI/CD pipeline to automate the construction and deployment of Docker images, enhancing the development efficiency.
- **TCP-Based Inter-Service Communication:** Employs TCP as the communication backbone among services, ensuring reliable and efficient data exchange for scalable system performance.